# Boilerplate: Symfony / React

**Decoupling front-end React JS with Symfony back-end API boiler plate.**

![Library logo](documentation/readme-assets/logo.png)

* * *

## :tada: Getting started

### Prerequisites

To be installed, and used, this project requires:

-   git
-   docker-compose

### Installation

**1. First, clone project repository**

```bash
git clone git@gitlab.com:phil-all/boilerplate-symfonyreact.git <your_project_name>
```

**2. Launch docker environment**

```bash
make start
```

**3. Symfony API**

From your root project, launch api bash:

```bash
make api
```

From api bash install composer packages:

```bash
composer install
```

API is accessible from `127.0.0.1:8700`

**4. React front app**

npm dependencies are installed when docker environment is launch.

Front app is accessible from `127.0.0.1:3000`

**5. Database**

Database port is 5432.

***

## Global Architecture

![Library architecture](documentation/readme-assets/architecture.png)
